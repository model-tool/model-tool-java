package name.wude.util;

import java.util.*;

/**
 * Represents a function that yields zero or more results from a given value.
 */
@FunctionalInterface
public interface Yielder<T> {

    /**
     * Yield zero or more results from a given value.
     */
    void apply(Queue<T> results, T value);

}
