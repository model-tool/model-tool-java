package name.wude.util;

import java.util.*;

/**
 * {@link Iterable Iterable} and {@link Iterator iterator} helper.
 */
public abstract class Iterables {

    /**
     * Create a {@link Iterable Iterable} and {@link Iterator iterator} helper.
     */
    private Iterables() {
    }

    /**
     * Get the first value in an {@link Iterable iterable} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param iterable     The {@link Iterable iterable} to check.
     * @param defaultValue The default value.
     * @return The first value in an {@link Iterable iterable} or a default value.
     */
    public static <T> T firstOrDefault(Iterable<T> iterable, T defaultValue) {
        for (T value : iterable) {
            return value;
        }
        return defaultValue;
    }

    /**
     * Get a single value in an {@link Iterable iterable} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param iterable     The {@link Iterable iterable} to check.
     * @param defaultValue The default value.
     * @return A single value in an {@link Iterable iterable} or a default value.
     */
    public static <T> T singleOrDefault(Iterable<T> iterable, T defaultValue) {
        Boolean valueTaken = false;
        T result = defaultValue;
        for (T value : iterable) {
            if (valueTaken) {
                return defaultValue;
            }
            result = value;
            valueTaken = true;
        }
        return result;
    }

}
