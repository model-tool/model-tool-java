package name.wude.util;

import java.util.*;
import java.util.function.*;

public final class Utils {

    private Utils() {
    }

    public static <S, T extends Comparable<T>> T max(Iterable<S> source, Function<S, T> converter,
            Comparator<T> comparator) {
        T targetItem = null;
        Iterator<S> sourceItor = source.iterator();
        if (sourceItor.hasNext()) {
            targetItem = converter.apply(sourceItor.next());
            while (sourceItor.hasNext()) {
                T tempItem = converter.apply(sourceItor.next());
                if (comparator.compare(targetItem, tempItem) > 0) {
                    tempItem = targetItem;
                }
            }
        }
        return targetItem;
    }

    public static <S, T extends Comparable<T>> T min(Iterable<S> source, Function<S, T> converter,
            Comparator<T> comparator) {
        T targetItem = null;
        Iterator<S> sourceItor = source.iterator();
        if (sourceItor.hasNext()) {
            targetItem = converter.apply(sourceItor.next());
            while (sourceItor.hasNext()) {
                T tempItem = converter.apply(sourceItor.next());
                if (comparator.compare(targetItem, tempItem) < 0) {
                    tempItem = targetItem;
                }
            }
        }
        return targetItem;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param <T>    The type of values.
     * @param first  The first value in question.
     * @param others The other values in question.
     * @return The first non-null value from the arguments.
     */
    @SafeVarargs
    public static <T> T coalesce(T first, T... others) {
        for (int i = 0; i < others.length && first == null; i++) {
            first = others[i];
        }
        return first;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param <T>    The type of values.
     * @param first  The first value in question.
     * @param others The other values in question.
     * @return The first non-null value from the arguments.
     */
    public static String coalesce(String first, String... others) {
        for (int i = 0; i < others.length && isNullOrEmpty(first); i++) {
            first = others[i];
        }
        return first;
    }

    /**
     * Create a {@link List list} from the given {@link Iterable iterable}.
     * 
     * @param <T>    The type of elements in the {@link List list} and
     *               {@link Iterable iterable}.
     * @param values The {@link Iterable iterable} the create the list from.
     * @return A {@link List list} from the given {@link Iterable iterable}.
     */
    public static <T> List<T> toList(Iterable<T> values) {
        List<T> result = new ArrayList<>();
        for (T value : values) {
            result.add(value);
        }
        return result;
    }

    /**
     * Get the substring of a given string beginning at the given start index and
     * ending at the given end index.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index where the substring starts. Negative indexes are
     *                   measured relative to the end of the given string.
     * @param endIndex   The index where the substring ends. Negative indexes are
     *                   measured relative to the end of the given string.
     * @return The substring of a given string beginning at the given index with the
     *         given length.
     */
    public static String substring(String value, int startIndex, int endIndex) {
        if (startIndex < 0) {
            startIndex += value.length();
        }
        if (endIndex < 0) {
            endIndex += value.length();
        }
        return value.substring(startIndex, endIndex);
    }

    /**
     * Get the substring of a given string beginning at the given index with the
     * given length.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index where the substring starts. Negative indexes are
     *                   measured relative to the end of the given string.
     * @param length     The length of the substring.
     * @return The substring of a given string beginning at the given index with the
     *         given length.
     */
    public static String substr(String value, int startIndex, int length) {
        if (startIndex < 0) {
            startIndex += value.length();
        }
        return value.substring(startIndex, startIndex + length);
    }

    /**
     * Get the substring of a given string beginning at the given index until the
     * end of the given string.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index whre the substring starts. Negative indexes are
     *                   measured relative to the end of the given string.
     * @return The substring of a given string beginning at the given index until
     *         the end of the given string.
     */
    public static String substr(String value, int startIndex) {
        int endIndex = value.length() - 1;
        if (startIndex < 0) {
            startIndex += endIndex + 1;
        }
        return value.substring(startIndex, endIndex);
    }

    /**
     * Get the string with the first letter upper case.
     * 
     * @param value The string to capitalize
     * @return The string with the first letter upper case.
     */
    public static String capitalize(String value) {
        if (!isNullOrWhiteSpace(value)) {
            return Character.toUpperCase(value.charAt(0)) + value.substring(1);
        }
        return null;
    }

    /**
     * Get the string with the first letter lower case.
     * 
     * @param value The string to uncapitalize
     * @return The string with the first letter lower case.
     */
    public static String uncapitalize(String value) {
        if (!isNullOrWhiteSpace(value)) {
            return Character.toLowerCase(value.charAt(0)) + value.substring(1);
        }
        return null;
    }

    /**
     * Is the string null or empty?
     * 
     * @param value The string to test.
     * @return Is the string null or empty?
     */
    public static boolean isNullOrEmpty(String value) {
        if (value == null) {
            return true;
        }
        if (value.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Is the string null, empty or whitespace?
     * 
     * @param value The string to test.
     * @return Is the string null or empty?
     */
    public static boolean isNullOrWhiteSpace(String value) {
        if (value == null) {
            return true;
        }
        if (value.isEmpty()) {
            return true;
        }
        if (value.trim().isEmpty()) {
            return true;
        }
        return false;
    }

}
