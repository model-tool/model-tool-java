package name.wude.util;

import java.util.*;
import java.util.function.*;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that is
 * qualified by a {@link Predicate predicate}.
 * 
 * @param <T> The concrete type of elements.
 */
public class OptionalAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected Iterable<? extends T> _iterable;
    protected Iterator<? extends T> _iterator;
    protected Predicate<T> _predicate;
    protected T _next = null;

    // endregion Fields

    // region Constructors

    /**
     * A proxy {@link Iterable iterable} and {@link Iterator iterator} that is
     * qualified by a {@link Predicate predicate}.
     * 
     * @param values    The values of the original {@link Iterable iterable}.
     * @param predicate The {@link Predicate predicate} by which to filter the
     *                  original {@link Iterable iterable}.
     */
    public OptionalAccess(Iterable<? extends T> values, Predicate<T> predicate) {
        this._iterable = values;
        this._predicate = predicate;
        this._iterator = null;
        this.reset();
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        this._iterator = this._iterable.iterator();
        this.fetchNext();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    /**
     * Fetches the next value.
     *
     * @return {@code true} if the iteration has more elements
     */
    private void fetchNext() {
        while (this._iterator.hasNext()) {
            T value = this._iterator.next();
            if (this._predicate.test(value)) {
                this._next = value;
                return;
            }
        }
        this._next = null;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        if (this._next == null) {
            this.fetchNext();
            return this._next != null;
        }
        return true;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        if (this._next != null) {
            T current = this._next;
            this.fetchNext();
            return current;
        }
        throw new NoSuchElementException();
    }

    // endregion Getters

}
