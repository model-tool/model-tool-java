package name.wude.util;

import java.util.*;
import java.util.function.*;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that iterates
 * in inverted order.
 * 
 * @param <T> The concrete type of elements.
 */
public class InvertedAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected List<? extends T> _list;
    protected int _index;

    // endregion Fields

    // region Constructors

    /**
     * Creates a proxy {@link Iterable iterable} and {@link Iterator iterator} that
     * iterates in inverted order.
     * 
     * @param values    The values of the original {@link Iterable iterable}.
     * @param predicate The {@link Predicate predicate} by which to filter the
     *                  original {@link Iterable iterable}.
     */
    public InvertedAccess(List<? extends T> list) {
        Objects.requireNonNull(list);
        this._list = list;
        this.reset();
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        this._index = this._list.size() - 1;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }
    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        return this._index >= 0;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        if (this._index >= 0) {
            return this._list.get(this._index--);
        }
        throw new NoSuchElementException();
    }

    // endregion Getters

}
