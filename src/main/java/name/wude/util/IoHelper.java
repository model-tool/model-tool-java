package name.wude.util;

import java.io.*;
import java.nio.file.*;
import java.nio.charset.StandardCharsets;
import java.net.URISyntaxException;

public final class IoHelper {

    private IoHelper() {
    }

    public static Writer getNewTextFileWriter(String filepath) throws FileNotFoundException {
        Writer writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(filepath), StandardCharsets.UTF_8));
        return writer;
    }

    public static Writer getNewTextFileWriter(Path filepath) throws FileNotFoundException {
        return new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(filepath.toString()), StandardCharsets.UTF_8));
    }

    public static Writer getNewTextFileWriter(File file) throws FileNotFoundException {
        return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
    }

    public static Path getFullFilePath(String fileName, String baseDirPath, String... pathParts) {
        return getDir(baseDirPath, pathParts).toPath().resolve(fileName).toAbsolutePath();
    }

    public static File getDir(String baseDirPath, String... pathParts) {
        File dir = new File(Paths.get(baseDirPath, pathParts).toString());

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File getDir(Path baseDirPath, String... pathParts) {
        return getDir(baseDirPath.toString(), pathParts);
    }

    public static Path getJarPath() throws URISyntaxException {
        File jarFile = new File(IoHelper.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        return jarFile.getParentFile().toPath();
    }

}
