package name.wude.util;

import java.util.*;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that merges
 * one or more iterable into one.
 * 
 * @param <T> The concrete type of elements.
 */
public class UnionAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected List<Iterable<? extends T>> _iterables;
    protected List<Iterator<? extends T>> _iterators;
    protected int _itorIdx;
    protected T _next = null;

    // endregion Fields

    // region Constructors

    /**
     * A proxy {@link Iterable iterable} and {@link Iterator iterator} that merges
     * one or more iterable into one.
     * 
     * @param values    The values of the original {@link Iterable iterable}.
     * @param predicate The {@link Predicate predicate} by which to filter the
     *                  original {@link Iterable iterable}.
     */
    public UnionAccess(List<Iterable<? extends T>> values) {
        this._iterables = values;
        this._iterators = new ArrayList<Iterator<? extends T>>(this._iterables.size());
        for (int i = 0; i < this._iterables.size(); i++) {
            this._iterators.add(this._iterables.get(i).iterator());
        }
    }

    /**
     * A proxy {@link Iterable iterable} that is qualified by a predicate.
     * 
     * @param values    The values of the original {@link Iterable iterable}.
     * @param predicate The {@link Predicate predicate} by which to filter the
     *                  original {@link Iterable iterable}.
     */
    @SafeVarargs
    public UnionAccess(Iterable<? extends T>... values) {
        this(Arrays.asList(values));
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        for (int i = 0; i < this._iterables.size(); i++) {
            this._iterators.set(i, this._iterables.get(i).iterator());
        }
        this._itorIdx = 0;
        this.fetchNext();
    }

    /**
     * Fetches the next value.
     *
     * @return {@code true} if the iteration has more elements
     */
    private void fetchNext() {
        for (; this._itorIdx < this._iterators.size(); this._itorIdx++) {
            while (this._iterators.get(this._itorIdx).hasNext()) {
                this._next = this._iterators.get(this._itorIdx).next();
                return;
            }
        }
        this._next = null;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        if (this._next == null) {
            this.fetchNext();
            return this._next != null;
        }
        return true;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        if (this._next != null) {
            T current = this._next;
            this.fetchNext();
            return current;
        }
        throw new NoSuchElementException();
    }

    // endregion Getters

}
