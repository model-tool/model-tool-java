package name.wude.model_tool.data;

import java.util.*;
import java.text.*;

public class ForeignKeyDefinition extends ConstraintDefinition {

    private String _name;
    private String _referencedTableName;
    private List<ForeignKeyColumnDefinition> _columns;

    public String getName() {
        return this._name;
    }

    public String getReferencedTableName() {
        return this._referencedTableName;
    }

    public List<ForeignKeyColumnDefinition> getColumns() {
        return this._columns;
    }

    public ForeignKeyDefinition(String name, String tableName, String referencedTableName,
            String constraintNameSuffix) {
        super(tableName + constraintNameSuffix + "Fk");
        this._name = name;
        this._referencedTableName = referencedTableName;
        this._columns = new ArrayList<>();
    }

    public void addColumn(String columnName, String referencedColumnName) {
        this._columns.add(new ForeignKeyColumnDefinition(columnName, referencedColumnName));
    }

    @Override
    public String toString() {
        return MessageFormat.format("Foreign Key \"{0}\"", this._constraintName);
    }

}
