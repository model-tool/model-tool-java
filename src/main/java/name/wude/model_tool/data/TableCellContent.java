package name.wude.model_tool.data;

import name.wude.util.*;

public class TableCellContent {

    private EntityAttributeDefinition _column;
    private String _value;

    public EntityAttributeDefinition getColumn() {
        return this._column;
    }

    public String getValue() {
        return this._value;
    }

    public TableCellContent(EntityAttributeDefinition column, String value) {
        this._column = column;
        if (value == null) {
            value = "";
        }
        this._value = Utils.coalesce(value, "");
    }

}
