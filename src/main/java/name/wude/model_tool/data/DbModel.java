package name.wude.model_tool.data;

import name.wude.util.*;

import java.util.*;

/**
 * Represents a database model.
 */
public class DbModel {

    private String _codeLanguage;
    private String _dbSystemCode;
    private String _modelName;
    private String _modelNamespace;
    private List<EntityDefinition> _entities = new ArrayList<EntityDefinition>();
    private HashMap<String, List<EntityDefinition>> _entitiesByClassNamespace = new HashMap<String, List<EntityDefinition>>();

    public String getCodeLanguage() {
        return this._codeLanguage;
    }

    public String getDbSystemCode() {
        return this._dbSystemCode;
    }

    public String getModelName() {
        return this._modelName;
    }

    public String getModelNamespace() {
        return this._modelNamespace;
    }

    public void setCodeLanguage(String value) {
        this._codeLanguage = value;
    }

    public void setDbSystemCode(String value) {
        this._dbSystemCode = value;
    }

    public void setModelName(String value) {
        this._modelName = value;
    }

    public void setModelNamespace(String value) {
        this._modelNamespace = value;
    }

    /**
     * Get the tables for this model.
     */
    public Iterable<EntityDefinition> getTables() {
        return new OneToManyAccess<>(this._entities, new Yielder<EntityDefinition>() {
            public void apply(Queue<EntityDefinition> results, EntityDefinition entity) {
                // Only non-replaced entities get a table.
                if (entity.getReplacingTable() == null) {
                    if (entity.getBaseTable() == null || entity.getBaseTable().getReplacingTable() == null) {
                        results.add(entity);
                        if (entity.getTextTable() != null) {
                            results.add(entity.getTextTable());
                        }
                    }
                } else {
                    results.add(entity.getReplacingTable());
                }
            }
        });
    }

    /**
     * Get the tables for this model in inverted order.
     */
    public Iterable<EntityDefinition> getTablesInverted() {
        return new InvertedAccess<>(Utils.toList(this.getTables()));
    }

    /**
     * Get the classes for this model.
     */
    public Iterable<EntityDefinition> getClasses() {
        return new OptionalAccess<>(this._entities, (entity) -> !entity.isTextEntity());
    }

    public DbModel(String codeLanguage, String dbSystemCode, String modelName, String modelNamespace,
            List<EntityDefinition> entities) {
        this._codeLanguage = codeLanguage;
        this._dbSystemCode = dbSystemCode;
        this._modelName = modelName;
        this._modelNamespace = modelNamespace;
        this._entities = new ArrayList<EntityDefinition>();
        this._entitiesByClassNamespace = new HashMap<String, List<EntityDefinition>>();
        if (entities != null && entities.size() > 0) {
            entities.forEach((v) -> this.addEntity(v));
        }
    }

    public DbModel() {
        this(null, null, null, null, null);
    }

    public List<EntityDefinition> getEntitiesByClassNamespace(String classNamespace) {
        List<EntityDefinition> entities = this._entitiesByClassNamespace.get(classNamespace);
        if (entities != null) {
            return entities;
        }
        return new ArrayList<EntityDefinition>();
    }

    public void addEntity(EntityDefinition entity) {
        String namespace = Utils.coalesce(entity.getClassNamespace(), this._modelNamespace);
        if (!this._entitiesByClassNamespace.containsKey(namespace)) {
            this._entitiesByClassNamespace.put(namespace, new ArrayList<EntityDefinition>());
        }
        this._entitiesByClassNamespace.get(namespace).add(entity);
        this._entities.add(entity);
    }

    @Override
    public String toString() {
        return "DbModel";
    }

}
