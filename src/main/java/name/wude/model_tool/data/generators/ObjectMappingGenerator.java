package name.wude.model_tool.data.generators;

import name.wude.model_tool.data.*;

import java.io.*;

public abstract class ObjectMappingGenerator {

  protected final static String encoding = "UTF-8";

  protected ObjectMappingGenerator() {
  }

  public abstract String getFileExtension();

  public abstract void writeContextClass(Writer writer, DbModel model) throws IOException;

  public abstract void writeTableClass(Writer writer, String contextName, EntityDefinition table) throws IOException;

  public abstract void writeRowClass(Writer writer, EntityDefinition table) throws IOException;

}
