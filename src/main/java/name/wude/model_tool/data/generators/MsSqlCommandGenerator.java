package name.wude.model_tool.data.generators;

import name.wude.model_tool.data.*;
import name.wude.util.*;

import org.apache.commons.lang3.StringUtils;

import org.apache.velocity.*;
import org.apache.velocity.app.*;
import org.apache.velocity.runtime.*;
import org.apache.velocity.runtime.resource.loader.*;

import java.util.*;
import java.text.*;
import java.io.*;

public class MsSqlCommandGenerator extends SqlCommandGenerator {

    private final static String modelTpl = "name/wude/model_tool/data/generators/templates/MsSqlModel.sql.vm";
    // private final static String dataTpl =
    // "name/wude/model_tool/data/generators/templates/MsSqlData.sql.vm";

    private VelocityEngine engine = new VelocityEngine();

    public MsSqlCommandGenerator(Writer writer) {
        super(writer);
        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        engine.init();
    }

    @Override
    public void getCreateDatabaseCommands(String dbName, Iterable<EntityDefinition> tables) throws IOException {
        try {
            VelocityContext context = new VelocityContext();
            context.put("generator", this);
            context.put("dbName", dbName);
            context.put("tables", tables);

            Template template = this.engine.getTemplate(modelTpl, encoding);
            template.merge(context, this.writer);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void getInsertRowCommands(DbModel model, Collection<TableContent> tableContents) throws IOException {
        try {
            VelocityContext context = new VelocityContext();
            context.put("generator", this);
            context.put("model", model);
            context.put("tableContents", tableContents);

            Template template = this.engine.getTemplate(modelTpl, encoding);
            template.merge(context, this.writer);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static String getObjectTypeCode(EntityDefinition dbObject) {
        if (dbObject instanceof EntityDefinition) {
            return "U";
        }
        return "";
    }

    protected String getDataTypes(EntityAttributeDefinition column) {
        String dataType = "";
        switch (column.getDataType()) {
        case UUID:
            dataType = "binary(16)";
            break;
        case I8:
            dataType = "tinyint";
            break;
        case I16:
            dataType = "smallint";
            break;
        case I32:
            dataType = "int";
            break;
        case I64:
            dataType = "bigint";
            break;
        case UI8:
            dataType = "unsigned tinyint";
            break;
        case UI16:
            dataType = "unsigned smallint";
            break;
        case UI32:
            dataType = "unsigned int";
            break;
        case UI64:
            dataType = "unsigned bigint";
            break;
        case F32:
            dataType = "float";
            break;
        case F64:
            dataType = "double";
            break;
        case N:
            dataType = MessageFormat.format("decimal({0}, {1})", Utils.coalesce(column.getLength(), 0),
                    Utils.coalesce(column.getPrecision(), 0));
            break;
        case D:
            dataType = "date";
            break;
        case DT:
            dataType = "datetime";
            break;
        case T:
            dataType = "time";
            break;
        case B:
            dataType = MessageFormat.format("binary({0})", Utils.coalesce(column.getLength(), 0));
            break;
        case VB:
            dataType = MessageFormat.format("varbinary({0})", Utils.coalesce(column.getLength(), 0));
            break;
        case C:
            dataType = MessageFormat.format("char({0})", Utils.coalesce(column.getLength(), 0));
            break;
        case VC:
            dataType = MessageFormat.format("varchar({0})", Utils.coalesce(column.getLength(), 0));
            break;
        default:
            dataType = "";
        }
        return StringUtils.rightPad(dataType, this.maxDataTypeLen);
    }

}
