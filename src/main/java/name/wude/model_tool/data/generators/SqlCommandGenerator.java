package name.wude.model_tool.data.generators;

import name.wude.model_tool.data.*;

import java.io.*;
import java.util.*;

public abstract class SqlCommandGenerator {

    protected final static String encoding = "UTF-8";

    protected Writer writer;
    protected int maxColNameLen;
    protected int maxDataTypeLen;

    protected SqlCommandGenerator(Writer writer) {
        this.writer = writer;
    }

    public abstract void getCreateDatabaseCommands(String dbName, Iterable<EntityDefinition> tables) throws IOException;

    public abstract void getInsertRowCommands(DbModel model, Collection<TableContent> tableContents) throws IOException;

}
