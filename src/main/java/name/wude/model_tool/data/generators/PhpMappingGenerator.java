package name.wude.model_tool.data.generators;

import name.wude.model_tool.data.*;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.velocity.*;
import org.apache.velocity.app.*;
import org.apache.velocity.runtime.*;
import org.apache.velocity.runtime.resource.loader.*;

import java.io.*;

public class PhpMappingGenerator extends ObjectMappingGenerator {

    private final static String contextTpl = "name/wude/model_tool/data/generators/templates/PhpContext.php.vm";
    private final static String tableTpl = "name/wude/model_tool/data/generators/templates/PhpTable.php.vm";
    private final static String rowTpl = "name/wude/model_tool/data/generators/templates/PhpRow.php.vm";

    private VelocityEngine engine = new VelocityEngine();

    public PhpMappingGenerator() {
        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        engine.init();
    }

    @Override
    public String getFileExtension() {
        return "php";
    }

    @Override
    public void writeContextClass(Writer writer, DbModel model) throws IOException {
        try {
            EntityDefinition userTable = null;
            for (EntityDefinition table : model.getTables()) {
                if (table.getTableName().equals("Users")) {
                    userTable = table;
                }
            }
            if (userTable == null) {
                throw new InvalidOperationException("No user table could be identified.");
            }

            VelocityContext context = new VelocityContext();
            context.put("generator", this);
            context.put("model", model);
            context.put("userTable", userTable);

            Template template = this.engine.getTemplate(contextTpl, encoding);
            template.merge(context, writer);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void writeTableClass(Writer writer, String contextName, EntityDefinition table) throws IOException {
        try {
            VelocityContext context = new VelocityContext();
            context.put("generator", this);
            context.put("contextName", contextName);
            context.put("table", table);

            Template template = this.engine.getTemplate(tableTpl, encoding);
            template.merge(context, writer);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void writeRowClass(Writer writer, EntityDefinition table) throws IOException {
        try {
            VelocityContext context = new VelocityContext();
            context.put("generator", this);
            context.put("table", table);

            Template template = this.engine.getTemplate(rowTpl, encoding);
            template.merge(context, writer);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public String getDataTypePascalCase(EntityAttributeDefinition column) {
        return getDataType(column, false);
    }

    public String getDataTypeCamelCase(EntityAttributeDefinition column) {
        return getDataType(column, true);
    }

    public static String getDataType(EntityAttributeDefinition column, boolean isLower) {
        String dataType = "";
        switch (column.getDataType()) {
        case UUID:
            dataType = "Uuid";
            break;
        case I8:
        case I16:
        case I32:
            dataType = isLower ? "int" : "Int";
            break;
        case N:
        case F32:
        case F64:
            dataType = isLower ? "float" : "Float";
            break;
        case D:
        case DT:
        case TS:
            dataType = "DateTime";
            break;
        case T:
            dataType = "Time";
            break;
        case VB:
        case B:
        case VC:
        case C:
        case LVC:
        case LC:
            dataType = isLower ? "string" : "String";
            break;
        default:
            dataType = "";
            break;
        }
        return dataType;
    }

}
