package name.wude.model_tool.data;

import java.util.*;

public abstract class DbProcedureDefinition extends DbObjectDefinition {

    public String getProcedureName() {
        return this.getObjectName();
    }

    protected DbProcedureDefinition(String procedureName) {
        super("P", "P_" + procedureName);
    }

    public static List<String> getGenericCatchBlock() {
        List<String> cmds = new ArrayList<String>();
        cmds.add("  begin catch");
        cmds.add("    declare @errno int");
        cmds.add("    declare @errmsg varchar(256)");
        cmds.add("    select  @errno  = error_number(),");
        cmds.add("            @errmsg = error_message()");
        cmds.add("    print concat('Fehler (', @errno, '): ', @errmsg)");
        cmds.add("  end catch");
        return cmds;
    }

    @Override
    public String toString() {
        return String.format("Database Procedure \"%s\"", this.getProcedureName());
    }

}
