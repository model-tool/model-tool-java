package name.wude.model_tool.data;

import java.util.*;
import java.text.*;

public class PrimaryKeyDefinition extends ConstraintDefinition {

    private List<EntityAttributeDefinition> _columns;

    public List<EntityAttributeDefinition> getColumns() {
        return this._columns;
    }

    public PrimaryKeyDefinition(String tableName, List<EntityAttributeDefinition> columns) {
        super(tableName + "Pk");
        this._columns = (columns != null ? columns : new ArrayList<>());
    }

    public PrimaryKeyDefinition(String tableName) {
        this(tableName, null);
    }

    public void addColumn(EntityAttributeDefinition column) {
        this._columns.add(column);
    }

    @Override
    public String toString() {
        return MessageFormat.format("Primary Key \"{0}\"", this.getConstraintName());
    }

}
