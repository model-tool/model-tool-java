package name.wude.model_tool.data;

import name.wude.model_tool.data.generators.*;

import java.io.*;

public final class DbFactory {

    private DbFactory() {
    }

    public static SqlCommandGenerator getGenerator(String dbSystemCode, Writer writer) {
        DbSystem dbSystem = DbSystem.valueOf(dbSystemCode);
        return getGenerator(dbSystem, writer);
    }

    public static SqlCommandGenerator getGenerator(DbSystem dbSystem, Writer writer) {
        SqlCommandGenerator commandGenerator;
        switch (dbSystem) {
        case MySql:
            commandGenerator = new MySqlCommandGenerator(writer);
            break;
        case MsSql:
            commandGenerator = new MsSqlCommandGenerator(writer);
            break;
        default:
            commandGenerator = null;
            break;
        }
        return commandGenerator;
    }

}
