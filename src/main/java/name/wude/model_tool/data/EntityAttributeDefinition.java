package name.wude.model_tool.data;

import name.wude.util.*;

import java.util.*;
import java.text.*;

/**
 * Represents the definition of an entity attribute.
 */
public class EntityAttributeDefinition {

    private int _columnId;
    private String _columnName;
    private String _description;
    private DbDataType _dataType;
    private int _length;
    private int _precision;
    private boolean _private;
    private boolean _optional;
    private boolean _primaryKey;
    private String _defaultValue;
    private String _foreignKeyName;
    private String _foreignKeyTable;
    private String _foreignKeyColumn;
    private String _uniqueKey;
    private String _checks;
    private Map<String, String> _localizedDescriptions;

    public int getColumnId() {
        return this._columnId;
    }

    public String getColumnName() {
        return this._columnName;
    }

    public String getColumnNamePascalCase() {
        return Utils.capitalize(this._columnName);
    }

    public String getColumnNameCamelCase() {
        return Utils.uncapitalize(this._columnName);
    }

    public String getDescription() {
        return this._description;
    }

    public DbDataType getDataType() {
        return this._dataType;
    }

    public int getLength() {
        return this._length;
    }

    public int getPrecision() {
        return this._precision;
    }

    public boolean isPrivate() {
        return this._private;
    }

    public boolean isOptional() {
        return this._optional;
    }

    public boolean isLocalized() {
        return this._dataType == DbDataType.LC || this._dataType == DbDataType.LVC;
    }

    public boolean hasDefaultValue() {
        return !Utils.isNullOrWhiteSpace(this._defaultValue);
    }

    public boolean isPrimaryKey() {
        return this._primaryKey;
    }

    public boolean isForeignKey() {
        return !Utils.isNullOrWhiteSpace(this._foreignKeyTable) || !Utils.isNullOrWhiteSpace(this._foreignKeyColumn);
    }

    public String getDefaultValue() {
        return this._defaultValue;
    }

    public String getForeignKeyName() {
        return this._foreignKeyName;
    }

    public String getForeignKeyTable() {
        return this._foreignKeyTable;
    }

    public String getForeignKeyColumn() {
        return this._foreignKeyColumn;
    }

    public String getUniqueKey() {
        return this._uniqueKey;
    }

    public String getChecks() {
        return this._checks;
    }

    public Map<String, String> getLocalizedDescriptions() {
        return this._localizedDescriptions;
    }

    public boolean hasUniqueKey() {
        return !Utils.isNullOrWhiteSpace(this._uniqueKey);
    }

    public boolean isId() {
        return this._dataType == DbDataType.UUID;
    }

    public EntityAttributeDefinition(int columnId, String columnName, String description, DbDataType dataType,
            int length, int precision, boolean $private, boolean optional, boolean primaryKey, String defaultValue,
            String foreignKeyName, String foreignKeyTable, String foreignKeyColumn, String checks, String uniqueKey,
            Map<String, String> localizedDescriptions) {
        this._columnId = columnId;
        this._columnName = columnName;
        this._description = description;
        this._dataType = dataType;
        this._length = Utils.coalesce(length, 0);
        this._precision = Utils.coalesce(precision, 0);
        this._private = Utils.coalesce($private, false);
        this._optional = Utils.coalesce(optional, false);
        this._primaryKey = Utils.coalesce(primaryKey, false);
        this._defaultValue = defaultValue;
        this._foreignKeyName = foreignKeyName;
        this._foreignKeyTable = foreignKeyTable;
        this._foreignKeyColumn = foreignKeyColumn;
        this._uniqueKey = uniqueKey;
        this._checks = checks;
        this._localizedDescriptions = localizedDescriptions;
    }

    public EntityAttributeDefinition(int columnId, String columnName, String description, DbDataType dataType,
            int length, int precision, boolean _private, boolean optional, boolean primaryKey, String defaultValue,
            String foreignKeyName, String foreignKeyTable, String foreignKeyColumn, String checks, String uniqueKey) {
        this(columnId, columnName, description, dataType, length, precision, _private, optional, primaryKey,
                defaultValue, foreignKeyName, foreignKeyTable, foreignKeyColumn, checks, uniqueKey,
                new HashMap<String, String>());
    }

    public EntityAttributeDefinition(int columnId, String columnName, String description, DbDataType dataType,
            int length, int precision, boolean _private, boolean optional, boolean primaryKey, String defaultValue,
            String foreignKeyName, String foreignKeyTable, String foreignKeyColumn) {
        this(columnId, columnName, description, dataType, length, precision, _private, optional, primaryKey,
                defaultValue, foreignKeyName, foreignKeyTable, foreignKeyColumn, null, null,
                new HashMap<String, String>());
    }

    @Override
    public String toString() {
        return MessageFormat.format("Table Column \"{0}\"", this._columnName);
    }

}
