package name.wude.model_tool.data;

public class CheckDefinition extends ConstraintDefinition {

  public String checkCode;

  public String getCheckCode() {
    return this.checkCode;
  }

  public CheckDefinition(String tableName, String columnName, String checkCode) {
    super(tableName + columnName + "Ck");
    this.checkCode = checkCode.replace("\r\n", " ").replace("\r", " ").replace("\n", " ");
  }

  @Override
  public String toString() {
    return String.format("Check \"%s\"", this._constraintName);
  }

}
