package name.wude.model_tool.data;

import name.wude.util.*;

import java.util.*;
import java.text.*;

/**
 * Represents the definition of an entity.
 */
public class EntityDefinition {

    private String _tableName;
    private String _tableNameCamelCase;
    private String _className;
    private String _classNamespace;
    private EntityDefinition _baseTable;
    private String _baseTableName;
    private String _baseClassName;
    private String _baseClassNamespace;
    private EntityDefinition _replacingTable;
    private String _singularDescription;
    private String _pluralDescription;
    private Map<String, String> _localizedSingularDescriptions;
    private Map<String, String> _localizedPluralDescriptions;
    private EntityDefinition _textTable;
    private List<EntityAttributeDefinition> _attributes;
    private PrimaryKeyDefinition _primaryKey;
    private Map<String, ForeignKeyDefinition> _foreignKeysByName;
    private Map<String, UniqueKeyDefinition> _uniqueKeysByName;
    private List<CheckDefinition> _checks;

    public String getTableName() {
        return this._tableName;
    }

    public String getTableNameCamelCase() {
        return this._tableNameCamelCase;
    }

    public String getClassName() {
        return this._className;
    }

    public String getClassNamespace() {
        return this._classNamespace;
    }

    public EntityDefinition getBaseTable() {
        return this._baseTable;
    }

    public String getBaseTableName() {
        return this._baseTableName;
    }

    public String getBaseClassName() {
        return this._baseClassName;
    }

    public String getBaseClassNamespace() {
        return this._baseClassNamespace;
    }

    public EntityDefinition getReplacingTable() {
        return this._replacingTable;
    }

    public String getSingularDescription() {
        return this._singularDescription;
    }

    public String getPluralDescription() {
        return this._pluralDescription;
    }

    public Map<String, String> getLocalizedSingularDescriptions() {
        return this._localizedSingularDescriptions;
    }

    public Map<String, String> getLocalizedPluralDescriptions() {
        return this._localizedPluralDescriptions;
    }

    public EntityDefinition getTextTable() {
        return this._textTable;
    }

    public List<EntityAttributeDefinition> getAttributes() {
        return this._attributes;
    }

    public PrimaryKeyDefinition getPrimaryKey() {
        return this._primaryKey;
    }

    public Iterable<ForeignKeyDefinition> getForeignKeys() {
        return this._foreignKeysByName.values();
    }

    public Iterable<UniqueKeyDefinition> getUniqueKeys() {
        return this._uniqueKeysByName.values();
    }

    public List<CheckDefinition> getChecks() {
        return this._checks;
    }

    /**
     * Get the table constraints for this entity.
     */
    public Iterable<ConstraintDefinition> getConstraints() {
        return new UnionAccess<>(Arrays.asList(this._primaryKey), this._uniqueKeysByName.values(),
                this.getForeignKeys(), this._checks);
    }

    /**
     * The table columns for this entity.
     */
    public Iterable<EntityAttributeDefinition> getColumns() {
        List<Iterable<? extends EntityAttributeDefinition>> iterables = new ArrayList<>();
        if (this._baseTable != null) {
            iterables.add(this._baseTable.getColumns());
        }
        iterables.add(new OptionalAccess<>(this._attributes, (attribute) -> this.isTextEntity()
                || (attribute.getDataType() != DbDataType.LC && attribute.getDataType() != DbDataType.LVC)));
        return new UnionAccess<>(iterables);
    }

    /**
     * The table columns for this entity by their names.
     */
    public Map<String, EntityAttributeDefinition> getColumnsByName() {
        HashMap<String, EntityAttributeDefinition> columnsByName = new HashMap<>();
        for (EntityAttributeDefinition column : this.getColumns()) {
            columnsByName.put(column.getColumnName(), column);
        }
        return columnsByName;
    }

    /**
     * The public, mandatory table columns for this entity.
     */
    public Iterable<EntityAttributeDefinition> getPublicMandatoryColumns() {
        return new OptionalAccess<>(this.getColumns(), (column) -> !column.isOptional() && !column.isPrivate());
    }

    /**
     * The public, mandatory, changable table columns for this entity.
     */
    public Iterable<EntityAttributeDefinition> getPublicMandatoryChangableColumns() {
        return new OptionalAccess<>(this.getColumns(), (column) -> !column.isOptional() && !column.isPrivate()
                && (column.isForeignKey() || !(column.isPrimaryKey() && column.getDataType() == DbDataType.UUID)));
    }

    /**
     * The class fields for this entity.
     */
    public Iterable<EntityAttributeDefinition> getFields() {
        Iterable<EntityAttributeDefinition> iterable;
        if (this.isTextEntity()) {
            iterable = new ArrayList<>();
        } else {
            iterable = this._attributes;
        }
        return iterable;
    }

    public Iterable<EntityAttributeDefinition> getNonPrimaryKeyColumns() {
        List<Iterable<? extends EntityAttributeDefinition>> iterables = new ArrayList<>();
        if (this._primaryKey == null) {
            iterables.add(this._attributes);
        }
        iterables.add(new OptionalAccess<>(this._attributes, (attr) -> !this._primaryKey.getColumns().contains(attr)));
        return new UnionAccess<>(iterables);
    }

    public boolean hasBaseTable() {
        return this._baseTable != null;
    }

    public boolean hasBaseTableName() {
        return !Utils.isNullOrWhiteSpace(this._baseTableName);
    }

    public boolean hasBaseClassName() {
        return !Utils.isNullOrWhiteSpace(this._baseClassName);
    }

    public boolean hasBaseClassNamespace() {
        return !Utils.isNullOrWhiteSpace(this._baseClassNamespace);
    }

    public boolean isReplaced() {
        return this._replacingTable != null;
    }

    public boolean hasLocalizedText() {
        return this._textTable != null;
    }

    public boolean isTextEntity() {
        return Utils.substr(this._className, -4, 4).equals("Text");
    }

    public EntityDefinition(String tableName, String className, String classNamespace, EntityDefinition baseTable,
            String baseTableName, String baseClassName, String baseClassNamespace, String singularDescription,
            String pluralDescription, Map<String, String> localizedSingularDescriptions,
            Map<String, String> localizedPluralDescriptions, List<EntityAttributeDefinition> attributes) {
        this._tableName = tableName;
        this._tableNameCamelCase = Utils.uncapitalize(tableName);
        this._className = className;
        this._classNamespace = classNamespace;
        this._baseTable = baseTable;
        this._baseTableName = baseTableName;
        this._baseClassName = baseClassName;
        this._baseClassNamespace = baseClassNamespace;
        this._replacingTable = null;
        this._singularDescription = singularDescription;
        this._pluralDescription = pluralDescription;
        this._localizedSingularDescriptions = localizedSingularDescriptions;
        this._localizedPluralDescriptions = localizedPluralDescriptions;
        this._textTable = null;
        this._attributes = new ArrayList<EntityAttributeDefinition>();
        this._primaryKey = new PrimaryKeyDefinition(tableName);
        this._foreignKeysByName = new HashMap<>();
        this._uniqueKeysByName = new HashMap<>();
        this._checks = new ArrayList<CheckDefinition>();

        if (this._baseTable != null) {
            if (this._baseTable._tableName.equals(this._tableName)) {
                this._baseTable._replacingTable = this;
            }
            for (EntityAttributeDefinition column : this._baseTable._primaryKey.getColumns()) {
                this._primaryKey.addColumn(column);
            }
        }

        if (attributes != null && attributes.size() > 0) {
            for (EntityAttributeDefinition attribute : attributes) {
                this.addAttribute(attribute);
            }
        }
    }

    public EntityDefinition(String tableName, String className, String classNamespace, EntityDefinition baseTable,
            String baseTableName, String baseClassName, String baseClassNamespace, String singularDescription,
            String pluralDescription, Map<String, String> localizedSingularDescriptions,
            Map<String, String> localizedPluralDescriptions) {
        this(tableName, className, classNamespace, baseTable, baseTableName, baseClassName, baseClassNamespace,
                singularDescription, pluralDescription, localizedSingularDescriptions, localizedPluralDescriptions,
                new ArrayList<>());
    }

    public EntityDefinition(String tableName, String className, String classNamespace, EntityDefinition baseTable,
            String baseTableName, String baseClassName, String baseClassNamespace, String singularDescription,
            String pluralDescription) {
        this(tableName, className, classNamespace, baseTable, baseTableName, baseClassName, baseClassNamespace,
                singularDescription, pluralDescription, new HashMap<>(), new HashMap<>(), new ArrayList<>());
    }

    public void addAttribute(EntityAttributeDefinition attribute) {
        this._attributes.add(attribute);
        if (attribute.isPrimaryKey()) {
            this._primaryKey.addColumn(attribute);
            if (this._textTable != null) {
                this._textTable.addAttribute(attribute);
            }
        }
        if (attribute.isForeignKey()) {
            String fkn = Utils.coalesce(attribute.getForeignKeyName(), attribute.getForeignKeyTable());
            if (!this._foreignKeysByName.containsKey(fkn)) {
                this._foreignKeysByName.put(fkn,
                        new ForeignKeyDefinition(attribute.getForeignKeyName(), this._tableName,
                                attribute.getForeignKeyTable(), Utils.coalesce(attribute.getForeignKeyName(),
                                        Utils.substring(attribute.getColumnName(), 0, -2))));
            }
            this._foreignKeysByName.get(fkn).addColumn(attribute.getColumnName(), attribute.getForeignKeyColumn());
        }
        if (!Utils.isNullOrWhiteSpace(attribute.getUniqueKey())) {
            UniqueKeyDefinition uniqueKey;
            if (!this._uniqueKeysByName.containsKey(attribute.getUniqueKey())) {
                uniqueKey = new UniqueKeyDefinition(attribute.getUniqueKey(), this.getTableName());
                this._uniqueKeysByName.put(attribute.getUniqueKey(), uniqueKey);
            } else {
                uniqueKey = this._uniqueKeysByName.get(attribute.getUniqueKey());
            }
            uniqueKey.addColumn(attribute);
        }
        if (!Utils.isNullOrWhiteSpace(attribute.getChecks())) {
            this._checks
                    .add(new CheckDefinition(this.getTableName(), attribute.getColumnName(), attribute.getChecks()));
        }

        // It's a localizable column,
        // that has to be outsourced into the associated text table.
        if (!this.isTextEntity()
                && (attribute.getDataType() == DbDataType.LC || attribute.getDataType() == DbDataType.LVC)) {
            if (this._textTable == null) {
                this._textTable = new EntityDefinition(MessageFormat.format("{0}Texts", this._className),
                        MessageFormat.format("{0}Text", this._className), this._classNamespace, null, null, null, null,
                        MessageFormat.format("{0} Text", this._singularDescription),
                        MessageFormat.format("{0} Texts", this._singularDescription));
                int cx = 1;
                for (EntityAttributeDefinition column : this._primaryKey.getColumns()) {
                    // this._textTable.addAttribute(column);
                    this._textTable.addAttribute(new EntityAttributeDefinition(cx++, column.getColumnName(),
                            column.getDescription(), column.getDataType(), column.getLength(), column.getPrecision(),
                            false, false, true, null, null, this._tableName, column.getColumnName()));
                }
                this._textTable.addAttribute(new EntityAttributeDefinition(1, "LanguageCode", "Language Code",
                        DbDataType.C, 2, 0, false, false, true, null, null, "Languages", "LanguageCode"));
            }
            this._textTable.addAttribute(attribute);
        }
    }

    @Override
    public String toString() {
        return MessageFormat.format("Table '{0}'", this._tableName);
    }

}
