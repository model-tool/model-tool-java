package name.wude.model_tool.data;

import java.util.*;

public class TableRowContent {

    private EntityDefinition _tableDefinition;
    private Collection<EntityAttributeDefinition> _columns;
    private Collection<TableCellContent> _cells;

    public EntityDefinition getTableDefinition() {
        return this._tableDefinition;
    }

    public Collection<EntityAttributeDefinition> getColumnDefinitions() {
        return this._columns;
    }

    public Collection<TableCellContent> getCells() {
        return this._cells;
    }

    public TableRowContent(EntityDefinition tableDefinition, Collection<EntityAttributeDefinition> columns,
            Collection<TableCellContent> cells) {
        this._tableDefinition = tableDefinition;
        this._columns = columns;
        this._cells = (cells != null ? cells : null);
    }

    public TableRowContent(EntityDefinition tableDefinition, Collection<EntityAttributeDefinition> columns,
            TableCellContent... cells) {
        this(tableDefinition, columns, Arrays.asList(cells));
    }

}
