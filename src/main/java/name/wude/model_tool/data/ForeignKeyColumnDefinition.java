package name.wude.model_tool.data;

public class ForeignKeyColumnDefinition {

    private String _columnName;
    private String _referencedColumnName;

    public String getColumnName() {
        return this._columnName;
    }

    public String getReferencedColumnName() {
        return this._referencedColumnName;
    }

    public ForeignKeyColumnDefinition(String columnName, String referencedColumnName) {
        this._columnName = columnName;
        this._referencedColumnName = referencedColumnName;
    }

}
