package name.wude.model_tool.data;

public abstract class DbObjectDefinition {

    private String objectType;
    private String objectName;

    public String getObjectType() {
        return this.objectType;
    }

    public String getObjectName() {
        return this.objectName;
    }

    public void setObjectName(String value) {
        this.objectName = value;
    }

    protected DbObjectDefinition(String objectType, String objectName) {
        this.objectName = objectName;
        this.objectType = objectType;
    }

    protected DbObjectDefinition(String objectType) {
        this(objectType, null);
    }

    @Override
    public String toString() {
        return String.format("Database Object \"%s\"", this.objectName);
    }
}
