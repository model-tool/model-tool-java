package name.wude.model_tool.data;

import java.util.*;
import java.text.*;

public class UniqueKeyDefinition extends ConstraintDefinition {

    private List<EntityAttributeDefinition> _columns;

    public List<EntityAttributeDefinition> getColumns() {
        return this._columns;
    }

    public UniqueKeyDefinition(String constraintName, String tableName) {
        super(tableName + constraintName + "Uq");
        this._columns = new ArrayList<EntityAttributeDefinition>();
    }

    public void addColumn(EntityAttributeDefinition columnName) {
        this._columns.add(columnName);
    }

    @Override
    public String toString() {
        return MessageFormat.format("Unique Key \"{0}\"", this._constraintName);
    }

}
