package name.wude.model_tool.data;

import name.wude.model_tool.data.generators.*;

public final class CodeFactory {

    private CodeFactory() {
    }

    public static ObjectMappingGenerator getGenerator(String codeLanguageCode) {
        CodeLanguage codeLanguage = CodeLanguage.valueOf(codeLanguageCode);
        return getGenerator(codeLanguage);
    }

    public static ObjectMappingGenerator getGenerator(CodeLanguage codeLanguage) {
        ObjectMappingGenerator commandGenerator;
        switch (codeLanguage) {
        case PHP:
            commandGenerator = new PhpMappingGenerator();
            break;
        default:
            commandGenerator = null;
            break;
        }
        return commandGenerator;
    }

}
