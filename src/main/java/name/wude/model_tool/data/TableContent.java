package name.wude.model_tool.data;

import java.util.*;

public class TableContent {

    private EntityDefinition _tableDefinition;
    private Collection<EntityAttributeDefinition> _columns;
    private List<TableRowContent> _rows;

    public EntityDefinition getTableDefinition() {
        return this._tableDefinition;
    }

    public Collection<EntityAttributeDefinition> getColumnDefinitions() {
        return this._columns;
    }

    public List<TableRowContent> getRows() {
        return this._rows;
    }

    public TableContent(EntityDefinition tableDefinition, Collection<EntityAttributeDefinition> columns,
            List<TableRowContent> rows) {
        this._tableDefinition = tableDefinition;
        this._columns = columns;
        this._rows = (rows != null ? rows : new ArrayList<>());
    }

    public TableContent(EntityDefinition tableDefinition, EntityAttributeDefinition... columns) {
        this(tableDefinition, Arrays.asList(columns), null);
    }

}
