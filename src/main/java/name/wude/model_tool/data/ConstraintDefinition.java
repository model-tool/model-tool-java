package name.wude.model_tool.data;

public abstract class ConstraintDefinition {

    public String _constraintName;

    public String getConstraintName() {
        return this._constraintName;
    }

    protected ConstraintDefinition(String constraintName) {
        this._constraintName = constraintName;
    }

    @Override
    public String toString() {
        return String.format("Constraint \"%s\"", this._constraintName);
    }

}
