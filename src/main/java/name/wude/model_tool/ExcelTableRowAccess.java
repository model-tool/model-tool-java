package name.wude.model_tool;

import name.wude.util.*;

import org.apache.poi.ss.usermodel.*;

import java.util.*;

public class ExcelTableRowAccess implements Iterable<ExcelTableRowAccess>, Iterator<ExcelTableRowAccess> {

    protected static DataFormatter _formatter = new DataFormatter();

    protected Sheet _sheet;
    protected List<String> _colNames;
    protected List<String> _langCodes;
    protected List<String> _usedLangCodes;
    protected List<Cell> _row;
    protected int _rx;
    protected int _initRx;
    protected boolean _isLocalized;
    protected boolean _hasNext;

    /**
     * The excel sheet that contains the underlying table.
     */
    public Sheet getSheet() {
        return this._sheet;
    }

    /**
     * The names of the columns in the underlying table.
     */
    public List<String> getColumnNames() {
        return this._colNames;
    }

    /**
     * The language codes paired to the column names by index.
     */
    public List<String> getLanguageCodes() {
        return this._langCodes;
    }

    /**
     * All codes of languages that were used to provide localized column values.
     */
    public List<String> getUsedLanguageCodes() {
        return this._usedLangCodes;
    }

    /**
     * The count of columns in the underlying table.
     */
    public int getColumnCount() {
        return this._colNames.size();
    }

    /**
     * The index of the current row in the underlying table.
     */
    public int getRowIndex() {
        return this._rx;
    }

    /**
     * Does the underlying table contain a localized column?
     */
    public boolean isLocalized() {
        return this._isLocalized;
    }

    protected ExcelTableRowAccess(Sheet sheet, int rowIndex) {
        this._sheet = sheet;
        this._row = null;
        this._initRx = rowIndex;
        this._colNames = new ArrayList<String>();
        this._langCodes = new ArrayList<String>();
        this._usedLangCodes = new ArrayList<String>();
        this._isLocalized = false;
        this._rx = rowIndex;
        this.reset();
    }

    protected ExcelTableRowAccess(Sheet sheet) {
        this(sheet, 0);
    }

    public void reset() {
        this._colNames = new ArrayList<String>();
        this._langCodes = new ArrayList<String>();
        this._usedLangCodes = new ArrayList<String>();
        this._isLocalized = false;
        this._rx = this._initRx;
        for (int cx = 0;; cx++) {
            String cellValue = this.getString(cx);
            if (Utils.isNullOrWhiteSpace(cellValue)) {
                break;
            }
            String langCode = null;
            if (cellValue.indexOf('(') == -1) {
                this._colNames.add(cellValue);
            } else {
                this._colNames.add(Utils.substr(cellValue, 0, cellValue.indexOf('(') - 1));
                this._isLocalized = true;
                langCode = Utils.substr(cellValue, cellValue.indexOf('(') + 1, 2);
            }
            this._langCodes.add(langCode);
            if (langCode != null && this._usedLangCodes.indexOf(langCode) == -1) {
                this._usedLangCodes.add(langCode);
            }
        }
        this._hasNext = !Utils.isNullOrWhiteSpace(getCellString(this._sheet, this._rx + 1, 0));
    }

    private void fetchNext() {
        this._hasNext = !Utils.isNullOrWhiteSpace(getCellString(this._sheet, this._rx + 1, 0));
        this._row = new ArrayList<Cell>();
        for (int cx = 0; cx < this._colNames.size(); cx++) {
            Cell cell = getCell(this._sheet, this._rx, cx);
            this._row.add(cell);
        }
        this._rx++;
    }

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        this.fetchNext();
        return this._hasNext;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public ExcelTableRowAccess next() {
        if (this._hasNext) {
            return this;
        }
        throw new NoSuchElementException();
    }

    private static Cell getCell(Sheet sheet, int rx, int cx) {
        try {
            Row row = sheet.getRow(rx);
            if (row != null) {
                return row.getCell(cx);
            }
        } catch (NullPointerException e) {
            // The caller must take care of the table dimensions and interpreting empty
            // values.
        }
        return null;
    }

    public static String getCellString(Sheet sheet, int rx, int cx) {
        return _formatter.formatCellValue(getCell(sheet, rx, cx));
    }

    protected String getString(int cx) {
        return _formatter.formatCellValue(getCell(this._sheet, this._rx, cx));
    }

    protected String getStringFlattened(int cx) {
        String text = this.getString(cx);
        if (!Utils.isNullOrWhiteSpace(text)) {
            text = text.replace(",\r\n", ", ");
            text = text.replace(",\r", ", ");
            text = text.replace(",\n", ", ");
            text = text.replace("\r\n", "");
            text = text.replace("\r", "");
            text = text.replace("\n", "");
        }
        return text;
    }

    protected boolean getBoolean(int cx) {
        String value = this.getString(cx);
        return value.equalsIgnoreCase("Y") ? true : false;
    }

    protected int getInteger(int cx) {
        Cell cell = getCell(this._sheet, this._rx, cx);
        if (cell != null) {
            return (int) cell.getNumericCellValue();
        }
        return 0;
    }

    /**
     * Returns an iterator over elements of type {@code ExcelTableRowAccess}.
     *
     * @return an Iterator.
     */
    public Iterator<ExcelTableRowAccess> iterator() {
        return this;
    }

}
