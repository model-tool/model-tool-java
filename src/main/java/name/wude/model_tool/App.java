package name.wude.model_tool;

import name.wude.util.*;

import java.nio.file.*;
import java.io.*;
import java.net.URISyntaxException;

import org.apache.poi.openxml4j.exceptions.*;
import org.apache.commons.cli.*;

public final class App {

  private App() {
  }

  public static void main(String[] args)
      throws ParseException, URISyntaxException, FileNotFoundException, IOException, InvalidFormatException {
    System.out.println("Start...");

    String workingDirPath = "../../";
    String baseModelDirPath = "./doc";
    String baseModelFileName = "Base-Relational-Model.xlsx";
    String baseDataFileName = "Base-Data.xlsx";
    String modelDirPath = "../test/doc/model";
    String modelFileName = "Relational-Model.xlsx";
    String dataFileName = "Data.xlsx";
    String sourceDirPath = "../test/src/";
    String modelSqlFileName = "Generated-Create-Tables.sql";
    String dataSqlFileName = "Generated-Insert-Rows.sql";

    CommandLine commandLine = getOptions(args);
    if (commandLine.hasOption("wd")) {
      workingDirPath = commandLine.getOptionValue("wd");
    }
    if (commandLine.hasOption("md")) {
      modelDirPath = commandLine.getOptionValue("md");
    }
    if (commandLine.hasOption("sd")) {
      sourceDirPath = commandLine.getOptionValue("sd");
    }
    if (commandLine.hasOption("mf")) {
      modelSqlFileName = commandLine.getOptionValue("mf");
    }

    workingDirPath = Paths.get(IoHelper.getJarPath().toString(), workingDirPath).toString();

    ModelProcessor modelProcessor = new ModelProcessor(workingDirPath, sourceDirPath, baseModelDirPath, modelDirPath,
        baseModelFileName, modelFileName, modelSqlFileName);
    modelProcessor.process();

    DataProcessor dataProcessor = new DataProcessor(modelProcessor, baseDataFileName, dataFileName, dataSqlFileName);
    dataProcessor.process();

    System.out.println("End.");
  }

  private static CommandLine getOptions(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(
        Option.builder("wd").longOpt("workdir").desc("The Working Directory").required(false).hasArg().build());
    options.addOption(
        Option.builder("md").longOpt("modeldir").desc("The Model Directory").required(false).hasArg().build());
    options.addOption(Option.builder("tf").longOpt("tabfile").desc("The Table File").required(false).hasArg().build());
    options.addOption(
        Option.builder("sd").longOpt("srcdir").desc("The Source Directory").required(false).hasArg().build());
    options.addOption(
        Option.builder("mf").longOpt("modelfile").desc("The Model Sql-File").required(false).hasArg().build());
    return new DefaultParser().parse(options, args);
  }
}
