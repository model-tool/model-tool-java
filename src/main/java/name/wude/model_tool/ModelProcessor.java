package name.wude.model_tool;

import name.wude.util.*;
import name.wude.model_tool.data.*;
import name.wude.model_tool.data.generators.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.openxml4j.exceptions.*;

import java.util.*;
import java.text.*;
import java.io.*;
import java.nio.file.*;

public final class ModelProcessor {

    private String _workingDirPath;
    private String _baseModelDirPath;
    private String _modelDirPath;
    private String _sourceDirPath;

    private String _baseModelFilePath;
    private String _modelFilePath;
    private String _createTablesFileName;

    private DbModel _model;

    public String getWorkingDirPath() {
        return this._workingDirPath;
    }

    public String getBaseModelDirPath() {
        return this._baseModelDirPath;
    }

    public String getModelDirPath() {
        return this._modelDirPath;
    }

    public String getSourceDirPath() {
        return this._sourceDirPath;
    }

    public DbModel getModel() {
        return this._model;
    }

    public String getCodeLanguage() {
        return this._model.getCodeLanguage();
    }

    public String getDbSystemCode() {
        return this._model.getDbSystemCode();
    }

    public String getModelName() {
        return this._model.getModelName();
    }

    public String getModelNamespace() {
        return this._model.getModelNamespace();
    }

    public Iterable<EntityDefinition> getTables() {
        return this._model.getTables();
    }

    public ModelProcessor(String workingDirPath, String sourceDirPath, String baseModelDirPath, String modelDirPath,
            String baseModelFileName, String modelFileName, String createTablesFileName) {
        this._workingDirPath = workingDirPath;
        this._baseModelDirPath = baseModelDirPath;
        this._modelDirPath = modelDirPath;
        this._baseModelFilePath = Paths.get(baseModelDirPath, baseModelFileName).toString();
        this._modelFilePath = Paths.get(modelDirPath, modelFileName).toString();
        this._sourceDirPath = sourceDirPath;
        this._createTablesFileName = createTablesFileName;
        this._model = null;
    }

    public void process() throws FileNotFoundException, IOException, InvalidFormatException {
        this._model = new DbModel();
        this.processExcelFile(this._baseModelFilePath);
        this.processExcelFile(this._modelFilePath);
        this.createSqlFiles(this._sourceDirPath, this._createTablesFileName);
        this.createCodeFiles(_sourceDirPath);
    }

    /**
     * Process the excel file with the given path.
     */
    private void processExcelFile(String excelFilePath)
            throws IOException, FileNotFoundException, InvalidFormatException {
        try (InputStream stream = new FileInputStream(excelFilePath)) {
            Workbook workbook = WorkbookFactory.create(stream);
            Sheet modelSheet = workbook.getSheet("Model");
            this._model.setCodeLanguage("PHP");
            this._model.setDbSystemCode(ExcelTableRowAccess.getCellString(modelSheet, 0, 1));
            this._model.setModelName(ExcelTableRowAccess.getCellString(modelSheet, 1, 1));
            this._model.setModelNamespace(ExcelTableRowAccess.getCellString(modelSheet, 2, 1));
            this.getDefinitions(workbook, modelSheet);
        }
    }

    private void getDefinitions(Workbook workbook, Sheet modelSheet) {
        for (ExcelTableRowAccess excelRow : new ExcelTableRowAccess(modelSheet, 4)) {
            int c = 0;
            String tableName = excelRow.getString(c++);
            Map<String, String> localizedSingularDescriptions = new HashMap<>();
            Map<String, String> localizedPluralDescriptions = new HashMap<>();
            this.getTable(tableName, // tableName
                    excelRow.getString(c++), // className
                    excelRow.getString(c++), // classNamespace
                    excelRow.getString(c++), // baseTableName
                    excelRow.getString(c++), // baseClassName
                    excelRow.getString(c++), // baseClassNamespace
                    excelRow.getStringFlattened(c++), // singularDescription
                    excelRow.getStringFlattened(c++), // pluralDescription
                    localizedSingularDescriptions, // localizedSingularDescriptions
                    localizedPluralDescriptions, // localizedPluralDescriptions
                    workbook.getSheet(tableName)); // sheet
            for (String languageCode : excelRow.getUsedLanguageCodes()) {
                // It is expected that two values of the same language
                // are listed in the right order!
                localizedSingularDescriptions.put(languageCode, excelRow.getStringFlattened(c++));
                localizedPluralDescriptions.put(languageCode, excelRow.getStringFlattened(c++));
            }
        }
    }

    /**
     * Get the information of an expected table.
     */
    private void getTable(String tableName, String className, String classNamespace, String baseTableName,
            String baseClassName, String baseClassNamespace, String singularDescription, String pluralDescription,
            Map<String, String> localizedSingularDescriptions, Map<String, String> localizedPluralDescriptions,
            Sheet sheet) {
        EntityDefinition baseTable = null;
        if (!Utils.isNullOrWhiteSpace(baseTableName) && !Utils.isNullOrWhiteSpace(baseClassNamespace)) {
            for (EntityDefinition table : this._model.getEntitiesByClassNamespace(baseClassNamespace)) {
                if (table.getTableName().equals(baseTableName)) {
                    baseTable = table;
                    break;
                }
            }
        }
        EntityDefinition entity = new EntityDefinition(tableName, className, classNamespace, baseTable, baseTableName,
                baseClassName, baseClassNamespace, singularDescription, pluralDescription,
                localizedSingularDescriptions, localizedPluralDescriptions);
        this._model.addEntity(entity);

        for (ExcelTableRowAccess excelRow : new ExcelTableRowAccess(sheet)) {
            int c = 0;
            String columnName = excelRow.getString(c++);
            String columnDescription = excelRow.getStringFlattened(c++);
            Map<String, String> localizedDescriptions = new HashMap<>();
            DbDataType dataType = DbDataType.valueOf(excelRow.getString(c++));
            entity.addAttribute(new EntityAttributeDefinition(excelRow.getRowIndex(), // columnId
                    columnName, columnDescription, dataType, // dataType
                    excelRow.getInteger(c++), // length
                    excelRow.getInteger(c++), // precision
                    excelRow.getBoolean(c++), // private
                    excelRow.getBoolean(c++), // optional
                    excelRow.getBoolean(c++), // primaryKey
                    excelRow.getString(c++), // defaultValue
                    excelRow.getString(c++), // foreignKeyName
                    excelRow.getString(c++), // foreignKeyTable
                    excelRow.getString(c++), // foreignKeyColumn
                    excelRow.getString(c++), // checks
                    excelRow.getString(c++), // uniqueKey
                    localizedDescriptions));
            c++; // Excel table column "Indexes" currently unused.
            for (String languageCode : excelRow.getUsedLanguageCodes()) {
                localizedDescriptions.put(languageCode, excelRow.getStringFlattened(c++));
            }
        }
    }

    private void createSqlFiles(String _sourceDirPath, String _createTablesFileName)
            throws FileNotFoundException, IOException {
        Path createTablesSqlFilePath = IoHelper.getFullFilePath(_createTablesFileName, _sourceDirPath, "sql");
        if (!Utils.isNullOrWhiteSpace(createTablesSqlFilePath.toString())) {
            createTablesSqlFilePath = createTablesSqlFilePath.toAbsolutePath();
            try (Writer writer = IoHelper.getNewTextFileWriter(createTablesSqlFilePath)) {
                SqlCommandGenerator sqlGenerator = DbFactory.getGenerator(this.getDbSystemCode(), writer);
                sqlGenerator.getCreateDatabaseCommands(this.getModelName(), this._model.getTables());
            }
        }
    }

    private void createCodeFiles(String _sourceDirPath) throws FileNotFoundException, IOException {
        ObjectMappingGenerator mappingGenerator = CodeFactory.getGenerator(this.getCodeLanguage());
        Path codeDirPath = IoHelper.getDir(_sourceDirPath, mappingGenerator.getFileExtension()).toPath();
        Path mappingPath = IoHelper.getDir(codeDirPath, this.getModelNamespace()).toPath();
        Path filepath = Paths.get(mappingPath.toString(),
                MessageFormat.format("{0}Context.{1}", this.getModelName(), mappingGenerator.getFileExtension()));
        try (Writer writer = IoHelper.getNewTextFileWriter(filepath)) {
            mappingGenerator.writeContextClass(writer, this._model);
        }
        for (EntityDefinition entity : this._model.getClasses()) {
            mappingPath = IoHelper.getDir(codeDirPath, entity.getClassNamespace()).toPath();
            filepath = Paths.get(mappingPath.toString(),
                    MessageFormat.format("{0}Table.{1}", entity.getClassName(), mappingGenerator.getFileExtension()));
            try (Writer writer = IoHelper.getNewTextFileWriter(filepath)) {
                mappingGenerator.writeTableClass(writer, this.getModelName(), entity);
            }
            filepath = Paths.get(mappingPath.toString(),
                    MessageFormat.format("{0}Row.{1}", entity.getClassName(), mappingGenerator.getFileExtension()));
            try (Writer writer = IoHelper.getNewTextFileWriter(filepath)) {
                mappingGenerator.writeRowClass(writer, entity);
            }
        }
    }

}
