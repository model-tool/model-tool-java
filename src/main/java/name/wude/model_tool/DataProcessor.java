package name.wude.model_tool;

import name.wude.util.*;
import name.wude.model_tool.data.*;
import name.wude.model_tool.data.generators.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.openxml4j.exceptions.*;

import java.util.*;
import java.io.*;
import java.nio.file.*;

public final class DataProcessor {

    private ModelProcessor _modelProcessor;
    private String _baseDataFilePath;
    private String _dataFilePath;
    private String _sqlFileName;
    private List<TableContent> _tableContents;

    public DataProcessor(ModelProcessor modelProcessor, String baseDataFileName, String dataFileName,
            String sqlFileName) {
        this._baseDataFilePath = Paths.get(modelProcessor.getBaseModelDirPath(), baseDataFileName).toString();
        this._dataFilePath = Paths.get(modelProcessor.getModelDirPath(), dataFileName).toString();
        this._modelProcessor = modelProcessor;
        this._sqlFileName = sqlFileName;
        this._tableContents = new ArrayList<>();
    }

    public void process() throws FileNotFoundException, IOException, InvalidFormatException {
        this.getData(this._baseDataFilePath);
        this.getDataFromTableDefinitions();
        this.getData(this._dataFilePath);
        this.createSqlFiles(this._modelProcessor.getSourceDirPath(), this._sqlFileName);
    }

    /**
     * Get the table data from the table definitions.
     */
    public void getDataFromTableDefinitions() {
        Map<String, EntityAttributeDefinition> columnsByName = null;
        // Table "Tables"
        TableContent tables = null;
        EntityAttributeDefinition tablesTableName = null;
        // Table "TableTexts"
        TableContent tableTexts = null;
        EntityAttributeDefinition tableTextsTableName = null;
        EntityAttributeDefinition tableTextsLanguageCode = null;
        EntityAttributeDefinition tableTextsTableDesc = null;
        EntityAttributeDefinition tableTextsClassDesc = null;
        // Table "Columns"
        TableContent columns = null;
        EntityAttributeDefinition columnsTableName = null;
        EntityAttributeDefinition columnsColumnName = null;
        // Table "ColumnTexts"
        TableContent columnTexts = null;
        EntityAttributeDefinition columnTextsTableName = null;
        EntityAttributeDefinition columnTextsColumnName = null;
        EntityAttributeDefinition columnTextsLanguageCode = null;
        EntityAttributeDefinition columnTextsColumnDesc = null;

        // Get the table definition for the tables containing the definitions.
        for (EntityDefinition tableDefinition : this._modelProcessor.getModel().getTables()) {
            switch (tableDefinition.getTableName()) {
            case "Tables":
                columnsByName = tableDefinition.getColumnsByName();
                tablesTableName = columnsByName.get("TableName");
                tables = new TableContent(tableDefinition, tablesTableName);
                break;
            case "TableTexts":
                columnsByName = tableDefinition.getColumnsByName();
                tableTextsTableName = columnsByName.get("TableName");
                tableTextsLanguageCode = columnsByName.get("LanguageCode");
                tableTextsTableDesc = columnsByName.get("TableDescription");
                tableTextsClassDesc = columnsByName.get("ClassDescription");
                tableTexts = new TableContent(tableDefinition, tableTextsTableName, tableTextsLanguageCode,
                        tableTextsTableDesc, tableTextsClassDesc);
                break;
            case "Columns":
                columnsByName = tableDefinition.getColumnsByName();
                columnsTableName = columnsByName.get("TableName");
                columnsColumnName = columnsByName.get("ColumnName");
                columns = new TableContent(tableDefinition, columnsTableName, columnsColumnName);
                break;
            case "ColumnTexts":
                columnsByName = tableDefinition.getColumnsByName();
                columnTextsTableName = columnsByName.get("TableName");
                columnTextsColumnName = columnsByName.get("ColumnName");
                columnTextsLanguageCode = columnsByName.get("LanguageCode");
                columnTextsColumnDesc = columnsByName.get("ColumnDescription");
                columnTexts = new TableContent(tableDefinition, columnTextsTableName, columnTextsColumnName,
                        columnTextsLanguageCode, columnTextsColumnDesc);
                break;
            default:
                break;
            }
        }

        if (tables != null && tableTexts != null && columns != null && columnTexts != null) {
            for (EntityDefinition tableDefinition : this._modelProcessor.getModel().getTables()) {
                if (!tableDefinition.isTextEntity()) {
                    tables.getRows().add(new TableRowContent(tables.getTableDefinition(), tables.getColumnDefinitions(),
                            new TableCellContent(tablesTableName, tableDefinition.getTableName())));
                    for (String languageCode : tableDefinition.getLocalizedSingularDescriptions().keySet()) {
                        tableTexts.getRows()
                                .add(new TableRowContent(tableTexts.getTableDefinition(),
                                        tableTexts.getColumnDefinitions(),
                                        new TableCellContent(tableTextsTableName, tableDefinition.getTableName()),
                                        new TableCellContent(tableTextsLanguageCode, languageCode),
                                        new TableCellContent(tableTextsTableDesc,
                                                tableDefinition.getLocalizedPluralDescriptions().get(languageCode)),
                                        new TableCellContent(tableTextsClassDesc,
                                                tableDefinition.getLocalizedSingularDescriptions().get(languageCode))));
                    }
                    for (EntityAttributeDefinition column : tableDefinition.getColumns()) {
                        columns.getRows()
                                .add(new TableRowContent(columns.getTableDefinition(), columns.getColumnDefinitions(),
                                        new TableCellContent(columnsTableName, tableDefinition.getTableName()),
                                        new TableCellContent(columnsColumnName, column.getColumnName())));
                        for (String languageCode : column.getLocalizedDescriptions().keySet()) {
                            columnTexts.getRows()
                                    .add(new TableRowContent(columnTexts.getTableDefinition(),
                                            columnTexts.getColumnDefinitions(),
                                            new TableCellContent(columnTextsTableName, tableDefinition.getTableName()),
                                            new TableCellContent(columnTextsColumnName, column.getColumnName()),
                                            new TableCellContent(columnTextsLanguageCode, languageCode),
                                            new TableCellContent(columnTextsColumnDesc,
                                                    column.getLocalizedDescriptions().get(languageCode))));
                        }
                    }
                    if (tableDefinition.hasLocalizedText()) {
                        tables.getRows().add(new TableRowContent(tables.getTableDefinition(),
                                tables.getColumnDefinitions(),
                                new TableCellContent(tablesTableName, tableDefinition.getTextTable().getTableName())));
                        for (EntityAttributeDefinition column : tableDefinition.getTextTable().getColumns()) {
                            columns.getRows()
                                    .add(new TableRowContent(columns.getTableDefinition(),
                                            columns.getColumnDefinitions(),
                                            new TableCellContent(columnsTableName,
                                                    tableDefinition.getTextTable().getTableName()),
                                            new TableCellContent(columnsColumnName, column.getColumnName())));
                            if (column.isLocalized()) {
                                for (String languageCode : column.getLocalizedDescriptions().keySet()) {
                                    columnTexts.getRows()
                                            .add(new TableRowContent(columnTexts.getTableDefinition(),
                                                    columnTexts.getColumnDefinitions(),
                                                    new TableCellContent(columnTextsTableName,
                                                            tableDefinition.getTextTable().getTableName()),
                                                    new TableCellContent(columnTextsColumnName, column.getColumnName()),
                                                    new TableCellContent(columnTextsLanguageCode, languageCode),
                                                    new TableCellContent(columnTextsColumnDesc,
                                                            column.getLocalizedDescriptions().get(languageCode))));
                                }
                            }
                        }
                    }
                }
            }
            if (tables.getRows().size() > 0) {
                this._tableContents.add(tables);
            }
            if (tableTexts.getRows().size() > 0) {
                this._tableContents.add(tableTexts);
            }
            if (columns.getRows().size() > 0) {
                this._tableContents.add(columns);
            }
            if (columnTexts.getRows().size() > 0) {
                this._tableContents.add(columnTexts);
            }
        }
    }

    private void getData(String dataFilePath) throws IOException, FileNotFoundException, InvalidFormatException {
        try (InputStream stream = new FileInputStream(dataFilePath)) {
            Workbook workbook = WorkbookFactory.create(stream);
            this.getContents(workbook);
        }
    }

    private void getContents(Workbook workbook) {
        for (EntityDefinition tableDefinition : this._modelProcessor.getModel().getTables()) {
            if (!tableDefinition.isTextEntity()) {
                ExcelTableRowAccess excelTable = new ExcelTableRowAccess(
                        workbook.getSheet(tableDefinition.getTableName()));
                Map<String, EntityAttributeDefinition> columnsByName = tableDefinition.getColumnsByName();
                Map<String, EntityAttributeDefinition> locColumnsByName = null;
                Map<Integer, EntityAttributeDefinition> colsByIndex = new HashMap<>();
                Map<String, Map<Integer, EntityAttributeDefinition>> locColsByIndex = new HashMap<>();
                if (excelTable.isLocalized()) {
                    locColumnsByName = tableDefinition.getTextTable().getColumnsByName();
                    for (String languageCode : excelTable.getUsedLanguageCodes()) {
                        Map<Integer, EntityAttributeDefinition> lc = new HashMap<>();
                        locColsByIndex.put(languageCode, lc);
                        lc.put(-1, locColumnsByName.get("LanguageCode"));
                    }
                }

                // Determine the columns to use.
                for (int cx = 0; cx < excelTable.getColumnCount(); cx++) {
                    String columnName = excelTable.getColumnNames().get(cx);
                    if (columnsByName.containsKey(columnName)) {
                        EntityAttributeDefinition column = columnsByName.get(columnName);
                        colsByIndex.put(cx, column);
                        if (excelTable.isLocalized() && column.isPrimaryKey()) {
                            // Primary key columns have to be used for texts too.
                            for (String languageCode : excelTable.getUsedLanguageCodes()) {
                                locColsByIndex.get(languageCode).put(cx, locColumnsByName.get(columnName));
                            }
                        }
                    } else if (excelTable.isLocalized() && locColumnsByName.containsKey(columnName)) {
                        String languageCode = excelTable.getLanguageCodes().get(cx);
                        locColsByIndex.get(languageCode).put(cx, locColumnsByName.get(columnName));
                    }
                }
                Collection<EntityAttributeDefinition> cols = colsByIndex.values();
                Collection<EntityAttributeDefinition> locCols = new ArrayList<>();
                List<TableRowContent> rows = new ArrayList<>();
                Map<String, List<TableRowContent>> locRows = new HashMap<>();
                if (excelTable.isLocalized()) {
                    for (String languageCode : excelTable.getUsedLanguageCodes()) {
                        if (locCols.size() == 0) {
                            locCols = locColsByIndex.get(languageCode).values();
                        }
                        locRows.put(languageCode, new ArrayList<>());
                    }
                }

                // Select the data.
                for (ExcelTableRowAccess excelRow : excelTable) {
                    List<TableCellContent> cells = new ArrayList<>();
                    Map<String, List<TableCellContent>> locCells = null;
                    if (excelRow.isLocalized()) {
                        locCells = new HashMap<>();
                        for (String languageCode : excelRow.getUsedLanguageCodes()) {
                            List<TableCellContent> lc = new ArrayList<>();
                            lc.add(new TableCellContent(locColumnsByName.get("LanguageCode"), languageCode));
                            locCells.put(languageCode, lc);
                        }
                    }
                    for (int cx = 0; cx < excelRow.getColumnCount(); cx++) {
                        if (colsByIndex.containsKey(cx)) {
                            EntityAttributeDefinition column = colsByIndex.get(cx);
                            String cellValue = excelRow.getString(cx);
                            cells.add(new TableCellContent(column, cellValue));
                            if (column.isPrimaryKey()) {
                                for (String languageCode : excelRow.getUsedLanguageCodes()) {
                                    if (locColsByIndex.get(languageCode).containsKey(cx)) {
                                        locCells.get(languageCode).add(new TableCellContent(
                                                locColsByIndex.get(languageCode).get(cx), cellValue));
                                    }
                                }
                            }
                        } else {
                            String languageCode = excelRow.getLanguageCodes().get(cx);
                            if (languageCode != null) {
                                if (locColsByIndex.get(languageCode).containsKey(cx)) {
                                    locCells.get(languageCode).add(new TableCellContent(
                                            locColsByIndex.get(languageCode).get(cx), excelRow.getString(cx)));
                                }
                            } else {
                                for (String languageCode2 : excelRow.getUsedLanguageCodes()) {
                                    if (locColsByIndex.get(languageCode2).containsKey(cx)) {
                                        locCells.get(languageCode2).add(new TableCellContent(
                                                locColsByIndex.get(languageCode2).get(cx), excelRow.getString(cx)));
                                    }
                                }
                            }
                        }
                    }
                    rows.add(new TableRowContent(tableDefinition, cols, cells));
                    if (excelRow.isLocalized()) {
                        for (String languageCode : excelRow.getUsedLanguageCodes()) {
                            locRows.get(languageCode).add(new TableRowContent(tableDefinition.getTextTable(), locCols,
                                    locCells.get(languageCode)));
                        }
                    }
                }
                if (rows.size() > 0) {
                    this._tableContents.add(new TableContent(tableDefinition, cols, rows));
                }
                if (excelTable.isLocalized()) {
                    List<TableRowContent> lr = new ArrayList<>();
                    for (String languageCode : excelTable.getUsedLanguageCodes()) {
                        lr.addAll(locRows.get(languageCode));
                    }
                    this._tableContents.add(new TableContent(tableDefinition.getTextTable(), locCols, lr));
                }
            }
        }
    }

    private void createSqlFiles(String sourceDirPath, String sqlFileName) throws FileNotFoundException, IOException {
        Path sqlFilePath = IoHelper.getFullFilePath(sqlFileName, sourceDirPath, "sql");
        if (!Utils.isNullOrWhiteSpace(sqlFilePath.toString())) {
            sqlFilePath = sqlFilePath.toAbsolutePath();
            try (Writer writer = IoHelper.getNewTextFileWriter(sqlFilePath)) {
                SqlCommandGenerator sqlGenerator = DbFactory.getGenerator(this._modelProcessor.getDbSystemCode(),
                        writer);
                sqlGenerator.getInsertRowCommands(this._modelProcessor.getModel(), this._tableContents);
            }
        }
    }

}
