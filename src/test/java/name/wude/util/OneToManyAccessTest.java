package name.wude.util;

import java.text.MessageFormat;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.assertTrue;

public class OneToManyAccessTest {

    @Test
    public void test() {
        OneToManyAccess<String> access = new OneToManyAccess<>(Arrays.asList("A", "XBC", "DEF"), new Yielder<String>() {
            public void apply(Queue<String> results, String value) {
                if (!value.contains("X")) {
                    for (int i = 0; i < value.length(); i++) {
                        results.add(MessageFormat.format("{0}", value.charAt(i)));
                    }
                }
            }
        });
        List<String> list = Utils.toList(access);
        assertTrue(list.size() == 4);
        assertTrue(list.get(0).equals("A"));
        assertTrue(list.get(1).equals("D"));
        assertTrue(list.get(2).equals("E"));
        assertTrue(list.get(3).equals("F"));
    }

}
